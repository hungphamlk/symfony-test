# Symfony JWT Authentication API
## Installation

    git clone https://gitlab.com/hungphamlk/symfony-test
    
    cd my_project
    composer install


## Running the Application


    symfony server:start
  

## API Routes

    - POST /register
    - POST /login
    - GET /user/{id}
    - POST /user
    - PUT /user/{id}
    - DELETE /user/{id}

[API Document](https://documenter.getpostman.com/view/4598834/2sA3QwcpiR#30430833-c283-4a9b-b856-3bd863cbbe1f)
    
